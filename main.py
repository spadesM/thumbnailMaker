from optparse import OptionParser
import imageX
from libraries_s import *
import cv2
import time

cwd = os.getcwd()
parser = OptionParser()
parser.add_option("-c", "--config", dest="config", help="Path to Parameters Config Path", default="config.json")
parser.add_option("-i", "--image", dest="onlyimage", help="Run With only one picture", default=False, action='store_true')
(options, args) = parser.parse_args()


def main_picture():
    params = config_params(options.config)
    img = cv2.imread(params.image)
    new_image = imageX.imagex(img, params, 1)
    new_image.write(params.logo)


def main_video():
    temp_usable_images = []
    params = config_params(options.config)
    cap = cv2.VideoCapture(params.video)
    video_totalframe = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    cnt = 0

    start_time = time.time()

    while True:
        progressbar(cnt, video_totalframe, start_time)
        ret, img = cap.read()
        cnt += 1
        if not ret:
            break
        # cv2.imwrite("outs/test" + str(cnt) + ".jpg", img)
        new_image = imageX.imagex(img, params, cnt)

        if new_image.isUsable:
            # cv2.imwrite(os.getcwd() + "/outs/" + str(cnt) + ".jpg", new_image.image_withparameters)
            if len(temp_usable_images) > 0:
                if is_similar(new_image.gray, temp_usable_images[len(temp_usable_images) - 1].gray):
                    temp_usable_images.append(new_image)
                else:
                    # cv2.imwrite(os.getcwd() + "/outs/" + str(cnt) + "param.jpg",
                    find_best_image(temp_usable_images).write(params.logo)
                    # cv2.imwrite(os.getcwd() + "/outs/" + str(cnt) + ".jpg", find_best_image(temp_usable_images).image)
                    temp_usable_images = [new_image]
            else:
                temp_usable_images.append(new_image)


if __name__ == '__main__':
    if options.onlyimage:
        main_picture()
    else:
        main_video()
