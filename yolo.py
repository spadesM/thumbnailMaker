import cv2
import numpy as np


class yolo:
    found_classes = []
    found_classes_confidence = []

    def __init__(self, net, classes, gpu=0):
        self.net = net
        self.classes = classes
        #self.net = cv2.dnn.readNet(weight_path, cfg_path)
        #self.classes = []
        #with open(classes_path, "r") as f:
        #    self.classes = [line.strip() for line in f.readlines()]

        self.net.setPreferableTarget(int(gpu))
        self.layer_names = self.net.getLayerNames()
        self.output_layers = [self.layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

    def predict(self, img):
        self.found_classes = []
        self.found_classes_confidence = []
        color = (0, 255, 0)

        width, height, channel = img.shape
        blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

        self.net.setInput(blob)
        outs = self.net.forward(self.output_layers)

        class_ids = []
        confidences = []
        boxes = []

        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)

                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        # print(indexes)
        number_object_detected = len(boxes)

        font = cv2.FONT_HERSHEY_PLAIN
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(self.classes[class_ids[i]])
                #cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                #cv2.putText(img, label, (x, y + 30), font, 3, color, 3)
                self.found_classes.append(label)
                self.found_classes_confidence.append("%.3f" %confidences[i])
        #cv2.imshow("asd", img)
        #cv2.waitKey(10000)

