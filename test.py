from optparse import OptionParser
import imageX
from libraries_s import *
import cv2

cwd = os.getcwd()
parser = OptionParser()
parser.add_option("-c", "--config", dest="config", help="Path to Parameters Config Path", default="config.txt")
(options, args) = parser.parse_args()


def main():

    params = config_params("config.json")
    img = cv2.imread("outs/2776.jpg")
    new_image = imageX.imagex(img, params, 1)
    new_image.write(params.logo)


if __name__ == '__main__':
    main()
