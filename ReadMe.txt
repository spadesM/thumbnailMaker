Çalıştırmak için 
	python main.py

Eğer parametreleri değiştirmek isterseniz config.json içerisinden değiştirebilirsin. Farklı bir json ile çalıştırmak
isterseniz;
	python main.py -c config.json

Eğer sadece bir fotoğraf ile denemek isterseniz (default:files/test.jpg)
	python main.py --image 

