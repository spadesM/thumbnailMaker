
import yolo
from libraries_s import *
import xml.etree.cElementTree as ET
import cv2
from xml.dom import minidom


class imagex:
    hasFace = False
    has_blur = False
    found_classes = []
    found_classes_confidence = []
    motion_blur_estimation = 0
    dominant_color = (0, 0, 0)
    brightness = 0
    # contrast = 0
    face_count = 0
    # blur_object = False
    open_eyes = True
    isUsable = True
    blur_threshold = 85
    write_results = False
    faces = 0

    def __init__(self, image, params, image_index):
        self.face_locations = []
        self.root = ET.Element("Thumbnail")
        self.image = image
        self.image_withparameters = image.copy()
        self.net = params.net
        self.classes = params.classes
        self.image_index = image_index
        self.gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        self.blur_detect()
        self.emotion_model = params.emotion_model
        self.emotion_labels = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
        if self.isUsable:
            self.find_face(params.face_predictor, params.face_cascade)
        if self.isUsable:
            self.find_objects()
            self.find_parameters()
            # self.write_parameters()
            # self.write()

    def find_parameters(self):
        self.dominant_color = getDominantColor(self.image)
        image_hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
        self.brightness = np.mean(image_hsv[:, :, 2])

    def write_parameters(self):
        texts = ["Face Count : " + str(self.face_count), "Found Classes : " + str(self.found_classes),
                 "Found Confidence : " + str(self.found_classes_confidence), "Motion Blur Estimation : " +
                 str(self.motion_blur_estimation), "Dominant Color : " + str(self.dominant_color), "Brightness : " +
                 str(self.brightness)]
        temp = 30
        for text in texts:
            cv2.putText(self.image_withparameters, text, (0, temp), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 1)
            temp += 30

        # cv2.imwrite(os.getcwd() + "/outs/"+str(self.image_index)+".jpg", self.image)

    def find_objects(self):
        YOLO = yolo.yolo(self.net, self.classes)
        YOLO.predict(self.image)
        self.found_classes = YOLO.found_classes
        self.found_classes_confidence = YOLO.found_classes_confidence

    def blur_detect(self):
        self.motion_blur_estimation = cv2.Laplacian(self.gray, cv2.CV_64F).var()

        if self.motion_blur_estimation < self.blur_threshold:
            self.has_blur = True
            # self.isUsable = False

    def write(self, logo=None):
        #cv2.imwrite('outs/'+str(self.image_index) + ".jpg", self.image)
        params = ET.SubElement(self.root, "Parameters")
        ET.SubElement(params, "Brightness").text = str(self.brightness)
        ET.SubElement(params, "MotionBlurEstimation").text = str(self.motion_blur_estimation)
        ET.SubElement(params, "DominantColor").text = str(self.dominant_color)
        found_objs = ET.SubElement(self.root, "FoundObjects")
        for idx_fo in range(len(self.found_classes)):
            ET.SubElement(found_objs, self.found_classes[idx_fo]).text = str(self.found_classes_confidence[idx_fo])
        # tree = ET.ElementTree(self.root)
        xmlstr = minidom.parseString(ET.tostring(self.root))
        with open('outs/'+str(self.image_index) + ".xml", "w") as f:
            f.write(xmlstr.toprettyxml())
        #cv2.imwrite('outs/'+str(self.image_index) + ".jpg", self.image)

        if logo is not None:
            box = self.getLogoLocation()
            cv2.imwrite('outs/'+str(self.image_index) + "final.jpg", addLogo2Image(self.image, logo, box))
        # tree.write('outs/'+str(self.image_index) + ".xml")

    def getLogoLocation(self):
        img_shape_x = self.image.shape[1]
        img_shape_y = self.image.shape[0]
        # xmin ymin xmax ymax
        box = [0,0,img_shape_x, img_shape_y]

        for inx in range(len(self.face_locations)):
            face_cx = (self.face_locations[inx][0].left() + self.face_locations[inx][0].right())/2.0
            face_cy = (self.face_locations[inx][0].top() + self.face_locations[inx][0].bottom())/2.0

            if face_cx < (box[0] + box[2])/2.0:
                box[0] = self.face_locations[inx][0].right()
            else:
                box[2] = self.face_locations[inx][0].left()

            if face_cy < (box[1] + box[3])/2.0:
                box[1] = self.face_locations[inx][0].bottom()
            else:
                box[3] = self.face_locations[inx][0].top()

        w = img_shape_x / 3
        h = img_shape_y / 3
        cx = (box[0] + box[2])/2.0
        cy = (box[1] + box[3])/2.0

        box = [int(cx-w/2), int(cy-h/2), int(cx+w/2), int(cy+h/2)]

        if box[0] < 0:
            box[2] += box[0]
            box[0] = 0
        if box[1] < 0:
            box[3] += box[1]
            box[1] = 0
        if box[2] > img_shape_x:
            box[0] = box[0] - box[2] + img_shape_x
            box[2] = img_shape_x - 1
        if box[3] > img_shape_y:
            box[1] = box[1] - box[3] + img_shape_y
            box[3] = img_shape_y - 1
        box_new = [box[1], box[3], box[0], box[2]]
        return box_new

    def detect_emotion(self, face_image):
        face_image = np.expand_dims(np.expand_dims(cv2.resize(face_image, (48, 48)), -1), 0)
        cv2.normalize(face_image, face_image, alpha=0, beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
        emotion_predict = self.emotion_model.predict(face_image)
        return np.max(emotion_predict), self.emotion_labels[int(np.argmax(emotion_predict))]

    def find_face(self, predictor, face_cascade):
        # eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
        self.faces = face_cascade.detectMultiScale(self.gray, 1.3, 5)
        xml_faces = ET.SubElement(self.root, "faces")
        # ET.SubElement(xml_faces, "Face Count", name="Face Count").text = str(len(self.faces))

        if len(self.faces) < 1:
            self.isUsable = False
            return

        faces, isHaveBlinks = face_blink_detection(self.gray, predictor)
        if isHaveBlinks:
            self.isUsable = False
            return

        if len(faces) > 0:
            self.hasFace = True
            self.face_count = len(faces)
            ET.SubElement(xml_faces, "FaceCount").text = str(len(self.faces))
            self.face_locations = faces
            for inx in range(len(faces)):
                temp_xml = ET.SubElement(xml_faces, "Face_"+str(inx+1))
                xmin = faces[inx][0].top()
                xmax = faces[inx][0].bottom()
                ymin = faces[inx][0].left()
                ymax = faces[inx][0].right()

                if xmin < 0:
                    xmin = 0
                if ymin < 0:
                    ymin = 0
                if xmax > self.gray.shape[0]:
                    xmax = self.gray.shape[0]
                if ymax > self.gray.shape[1]:
                    ymax = self.gray.shape[1]

                ET.SubElement(temp_xml, "Face_Location").text = str(ymin) + " " + str(xmin) + " " + str(ymax) + " " + str(xmax)
                precision, emotion = self.detect_emotion(self.gray[xmin:xmax, ymin:ymax])
                ET.SubElement(temp_xml, "FaceEmotion", emotion=str(emotion)).text = str(precision)
        else:
            self.isUsable = False
