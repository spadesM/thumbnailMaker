from scipy.stats.stats import pearsonr
import dlib
from scipy.spatial import distance as dist
from sklearn.cluster import KMeans
from imutils import face_utils
from operator import attrgetter
import sys
import numpy as np
import cv2
import json
import os
from keras.models import model_from_json
import time


class config_params:
    def __init__(self, json_path):
        with open(json_path, 'r') as f:
            cfg = json.load(f)
            cwd = os.getcwd()
            yolo_weight = cwd + "\\" + cfg["yolo_model"]
            yolo_cfg = cwd + "\\" + cfg["yolo_config"]
            self.net = cv2.dnn.readNet(yolo_weight, yolo_cfg)

            with open(cfg["classes"], "r") as f:
                classes = [line.strip() for line in f.readlines()]
            self.classes = class_configure(classes)
            self.face_predictor = dlib.shape_predictor(cfg["face_dat"])
            self.face_cascade = cv2.CascadeClassifier(cfg["face_cascade"])
            self.output = cfg["output"]
            self.video = cfg["video"]
            self.image = cfg["image"]
            self.logo = cv2.imread(cfg["logo"], cv2.IMREAD_UNCHANGED)

            json_file = open(cfg["emotion_json"], 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            self.emotion_model = model_from_json(loaded_model_json)
            self.emotion_model.load_weights(cfg["emotion_model"])


def addLogo2Image(src, logo, box, threshold=50):
    # box = [xmin, ymin, xmax, ymax]
    res = np.copy(src)
    if box[1]-box[0] < logo.shape[0]:
        ratio = logo.shape[0]/(box[1]-box[0])
        logo = cv2.resize(logo, (int(logo.shape[1]/ratio), int(logo.shape[0]/ratio)))
    if box[3]-box[2] < logo.shape[1]:
        ratio = logo.shape[1]/(box[3]-box[2])
        logo = cv2.resize(logo, (int(logo.shape[1]/ratio), int(logo.shape[0]/ratio)))

    start_point = (int((box[0]+box[1])/2 - logo.shape[0]/2), int((box[3]+box[2])/2 - logo.shape[1]/2))
    if len(logo.shape) == 2:
        for i in range(logo.shape[0]):
            for j in range(logo.shape[1]):
                res[start_point[0] + i][start_point[1] + j][0] = logo[i][j]
                res[start_point[0] + i][start_point[1] + j][1] = logo[i][j]
                res[start_point[0] + i][start_point[1] + j][2] = logo[i][j]
    elif len(logo.shape) == 3:
        if logo.shape[2] == 4:
            for i in range(logo.shape[0]):
                for j in range(logo.shape[1]):
                    if logo[i][j][3] > threshold:
                        res[start_point[0] + i][start_point[1] + j][0] = logo[i][j][0]
                        res[start_point[0] + i][start_point[1] + j][1] = logo[i][j][1]
                        res[start_point[0] + i][start_point[1] + j][2] = logo[i][j][2]
        else:
            for i in range(logo.shape[0]):
                for j in range(logo.shape[1]):
                    res[start_point[0] + i][start_point[1] + j][0] = logo[i][j][0]
                    res[start_point[0] + i][start_point[1] + j][1] = logo[i][j][1]
                    res[start_point[0] + i][start_point[1] + j][2] = logo[i][j][2]
    else:
        raise Exception("Invalid Logo")

    return res


def getDominantColor(img, dim4_threshold=50):
    kmeans = KMeans(n_clusters=1)
    image_cpy = img.copy()
    if image_cpy.shape[2] == 3:
        image_cpy = image_cpy.reshape((image_cpy.shape[0] * image_cpy.shape[1], 3))
        kmeans.fit(image_cpy)
        dominant_color = (int(kmeans.cluster_centers_[0][0]), int(kmeans.cluster_centers_[0][1]),
                          int(kmeans.cluster_centers_[0][2]))
        return dominant_color
    if image_cpy.shape[2] == 4:
        image_cpy = image_cpy.reshape((image_cpy.shape[0] * image_cpy.shape[1], 4))
        image_cpy = np.delete(image_cpy, np.argwhere(image_cpy[:, 1] < dim4_threshold), 0)
        kmeans.fit(image_cpy[:, :3])
        dominant_color = (int(kmeans.cluster_centers_[0][0]), int(kmeans.cluster_centers_[0][1]),
                          int(kmeans.cluster_centers_[0][2]))
        return dominant_color


def class_configure(class_list):
    bad_chars = ['&', '<', '>', "\`", "\"", " ", "&"]
    new_class_list = []
    for obj in class_list:
        for i in bad_chars:
            obj = obj.replace(i, '')
        new_class_list.append(obj)

    return new_class_list


def progressbar(cnt, size, start_time=None, bar_size=30):
    time_text = ""
    if start_time is not None:
        if cnt != 0:
            current_time = time.time() - start_time
            str_current = str(int(current_time / 60)) + ":" + str(int(current_time) % 60).zfill(2)
            remaining_time = (size-cnt)*current_time/cnt
            str_remaing = str(int(remaining_time / 60)) + ":" + str(int(remaining_time) % 60).zfill(2)
        else:
            str_current = ""
            str_remaing = ""
        time_text = " Time Spent: " + str_current + " Time Remaining: " + str_remaing

    temp = int(cnt / float(size) * bar_size)
    temp_nmr = int((cnt - temp * size / bar_size) * 10 / (size / bar_size))
    bar = "["

    for i in range(bar_size):
        if i < temp:
            bar += "#"
        elif i == temp:
            bar += str(temp_nmr)
        else:
            bar += " "
    bar = bar + "] " + str(cnt) + "/" + str(size) + time_text

    sys.stdout.write("\r \r {0}".format(str(bar)))


def find_best_image(images):
    if len(images) == 1:
        return images[0]
    if len(images) == 0:
        raise Exception("Image Array is Null")

    x = max(images, key=attrgetter('motion_blur_estimation'))
    return x


def is_similar(img1, img2, similarity_ratio=0.7):
    img1 = img1.flatten()
    img2 = img2.flatten()
    similarity, _ = pearsonr(img1.flatten(), img2.flatten())
    return similarity > similarity_ratio


def eye_aspect_ratio(eye):
    # compute the euclidean distances between the two sets of
    # vertical eye landmarks (x, y)-coordinates
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])

    # compute the euclidean distance between the horizontal
    # eye landmark (x, y)-coordinates
    C = dist.euclidean(eye[0], eye[3])

    # compute the eye aspect ratio
    ear = (A + B) / (2.0 * C)

    # return the eye aspect ratio
    return ear


def face_blink_detection(gray, predictor):
    EYE_AR_THRESH = 0.285
    detector = dlib.get_frontal_face_detector()
    (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    rects = detector(gray, 0)
    faces = []
    isHaveBlinks = False
    # loop over the face detections
    for rect in rects:
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        # extract the left and right eye coordinates, then use the
        # coordinates to compute the eye aspect ratio for both eyes
        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]
        leftEAR = eye_aspect_ratio(leftEye)
        rightEAR = eye_aspect_ratio(rightEye)

        # average the eye aspect ratio together for both eyes
        ear = (leftEAR + rightEAR) / 2.0
        # print(ear)
        if ear < EYE_AR_THRESH:
            # Blinked
            faces.append([rect, False])
            isHaveBlinks = True
        else:
            # Not Blinked
            faces.append([rect, True])

    return faces, isHaveBlinks
